# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Project 1 - Software Testing and Maintenance ###

Project 1 of Software Testing and Maintenance at University of Delaware (Spring 2015).

### TEAM ###

* Diogo Oliveira
* Krzysztof Czerwinski
* Ningjing Tian
* Scott Grauer-Gray

### Description ###

# Introduction #
This project centers on the open-source stringsearch library for Java. This library provides fast _string searching algorithms_, algorithms that efficiently search for instances of substrings within larger strings. The entirety of its source code is included in your project skeleton under the **src** directory.
 
To start, as usual, one member of each team should fork my [stringsearch repository](https://clause@bitbucket.org/clause/stringsearch) (forked repositories should be named: stringsearch-teamXX; where XX is the team number).  All team members, Steven Rhein, and myself should be added to the forked repo.
 
The layout of the project is as follows:
 
* **src** contains the source code of the stringsearch library.  _Do not modify._
 
* **test** contains the tests for stringsearch.
 
* **lib** contains necessary libraries. _Do not modify._
 
* **reports** contains various reports about the library and tests. _Deleted when project is cleaned._
 
* **build** contains compiled classes. Deleted when project is cleaned._
 
* **build.xml** the ant build file for the projects it has the following tasks:
 
* _build_ compiles the stringsearch library and tests
 
* _test_ executes the tests and generates an html report inside reports/test/html
 
* _coverage_ calculates the code coverage of the tests and generates an html report in reports/coverage
 
* _mutation_ calculate the mutation coverage of the test and generates an html report in reports/mutation
 
*  _javadoc_ generates the javadoc documentation for stringsearch in reports/javadoc
 
* _clean_ deletes the build and reports directories
 
Note that I would prefer you to use ant to build, test, coverage, etc. rather than Eclipse.  At least, you should make sure they and ant scripts still work before submitting.
 
# Submission artifacts
 
* A detail report explaining your testing process and its results. The report must include a link to your forked repository and a list of all team members. (Additional details of what should be in the - report are given in the following sections.)
 
* Tag your forked repository "final".  We will examine this tag when grading.
 
* Submit the report to the appropriate assignment on Sakai.
 
 
# Part 1 - Input-space partitioning
 
Your first task is to implement a number of tests based on input-space partitioning.  In the report, you should thoroughly document your choice of characteristics, partition schemes, blocks, constraints, etc.  More specifically, I want to see a tables (note the plural) similar to those used in the book and slides for each method, class, package, etc. that you are testing. You must also explain why, for each characteristic, your partition scheme is complete and disjoint.
 
After creating the necessary documentation, you should begin creating concrete JUnit tests from your blocks.  Recall that as testers, it is your responsibility to decide how much effort is necessary for testing your software.  Based on your limited time budget, choose a appropriate coverage criterion.  Note that you may choose to have different coverage criterion for each class or each method.  Justify your choices.
 
Each concrete test you create should be documented well.  I have provided an example test case.  Follow this style for all tests you write.
 
You can read about the requirements of the library by consulting the documentation created by running _ant javadoc_
 
Make sure that every test you write passes.
 
Note that at this point, you should not be any additional tools that you have learned (e.g., coverage, mutation, etc.; these will come later).
 
After creating the JUnit test cases, push everything to Bitbucket and tag the repository with the tag "input-space partitioning"
 
# Part 2 - Statement Coverage
 
You will now measure the _adequacy_ of your tests with respect to code coverage metrics. Then, you will enhance your tests.
 
Use the ant coverage task (or ecl emma, if necessary) to calculate the statement coverage of your tests.
 
Augment your test suite so that achieves as high a level of code coverage as necessary.
 
Make sure that every test you write passes.
 
New tests should be documented in a manner similar to how you documented the tests for Part 1.  The technique used should be code coverage and you should also indicate what previously uncovered lines of code were covered by the test.  You should also examine your partitions, blocks, and characteristics from Part 1 and attempt to identify why the uncovered lines of code were not covered.
 
In the report, you should indicate your initial level of code coverage, final level of code coverage, and a general description of why the uncovered code was not covered by the test created for Part 1.
 
When you are finished, push and tag the repository with the tag "statement coverage"
 
# Part 3 - Mutation coverage
 
You will now measure the _adequacy_ of your tests with respect to mutation metrics. Then, you will again enhance your tests.
 
Use the ant mutation task to calculate the mutation score of your tests.
 
Augment your test suite so that achieves as high a mutation score as possible
 
Make sure that every test you write passes.
 
New tests should be documented in a manner similar to how you documented the tests for Part 1.  The technique used should be code coverage and you should also indicate what previously uncovered lines of code were covered by the test.  You should also examine your partitions, blocks, and characteristics from Part 1 and attempt to identify why the uncovered lines of code were not covered.
 
In the report, you should indicate your initial level of code coverage, final level of code coverage, and a general description of why the uncovered code was not covered by the test created for Part 1 and Part 2.
 
When you are finished, push and tag the repository with the tag "statement coverage"
 
 
# Grading
 
## Report
 
I expect the report to be complete, legible, and grammatically correct.  You will lose points for missing information, poor / confusing formatting, and illegibility.
 
## Input-space partitioning
 
I expect to see at least twenty (20) tests in total, with more being perfectly reasonable. For this task, you will be graded on the following: 1) faithful adherence to and implementation of the input-space partitioning techniques learned in class; 2) the variety of your tests; 3) the effectiveness of your tests (which should be a given if you faithfully follow the techniques in class); and 4) your overall effort and organization.
 
## Statement coverage
 
You will be graded on how effective your enhanced tests are: the higher the coverage, the higher the grade. I have no predetermined scale as to what constitutes “adequate”. When grading, I will record the distribution of coverage values for the entire class and use a (very generous) curve to assign points. I will also reward creative and particularly elegant solutions.
 
## Mutation coverage
 
You will be graded on how effective your enhanced tests are: the higher the coverage, the higher the grade. I have no predetermined scale as to what constitutes “adequate”. When grading, I will record the distribution of coverage values for the entire class and use a curve to assign points.
 
## Extra credit
 
The stringsearch library contains at least a few bugs.  (Note that you will not find them by searching the Internet).  
 
You be will awarded bonus credit for each bug that you reveal with your tests.  Bugs that are found by a single team will be worth more than bugs reported by multiple teams.  Because all of your tests need to pass, annotate failure-revealing tests with @Ignore.  This will allow you to clearly document the failure, but not including failing tests in the test suite.
 
You do not need to attempt to understand or fix the bugs,  we'll take care of that during the debugging part of the course.
 
In addition to writing tests by hand, you may also want to write or use a fuzz testing tool as fuzz testing can be a particularly good way of revealing failures.